An example presenting how to use GitLab  Pipeline and Ansible to create Cisco ACI tenant. You can use any available method as long as it posts to the below URL - embeeded button, CGI script etc.
To trigger pipeline go to https://onlinecurl.com/ and paste the below URL. Set -X to POST.


> ```https://gitlab.com/api/v4/projects/13381413/ref/master/trigger/pipeline?token=432b70222625cab4ded66a38d77c5b``` 

Execution path:


> ```curl > GitLab Pipeline > Ansible Playbook (URI module) > Cisco ACI tenant Creation ``` 

Verify on Cisco ACI online always-on Sandbox under Tenant tab:


> ```
> https://sandboxapicdc.cisco.com
> username: admin
> password: ciscopsdt
> ```

Docker Image used:

> `solita/ansible-ssh` 

Final Ansible output:


> [](https://gitlab.com/grabarz666/my-first-gitlab-ansible-repo/wikis/uploads/57a9bcb498c7a48852046a18f6abceb4/4.PNG) 

